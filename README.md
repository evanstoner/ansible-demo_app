# Ansible Collection - evanstoner.demo_app

Documentation for the collection.

## Developing

Prerequisites:

- Python 3
- Podman

Create a new virtual environment for testing this collection:

```bash
VENV=/some/path
python3 -m venv $VENV
source $VENV/bin/activate
```

Now, in your virtual environment:

``` bash
# Install dependencies
pip install -r requirements.txt
# Run the default scenario...
molecule --base-config molecule/base.yml test
# ...or any other scenario
molecule --base-config molecule/base.yml test -s customized
```
